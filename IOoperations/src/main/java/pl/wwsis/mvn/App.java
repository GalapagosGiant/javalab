package pl.wwsis.mvn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class App 
{
	public static void main( String[] args )
    {
		// Zadanie 1-2.
		
		System.out.println("\n\tZadanie 1-2");
		
		String path = "../../dane1.txt";
		
		showFile(path);
		
		ArrayList<Integer> array = new ArrayList<Integer>();

	    readFile(array, path);
		
	    showSum(array);
	    
	    // Zadanie 3.
	    
	    System.out.println("\n\tZadanie 3");
	    
	    path = "../../dane2.txt";
		
	    try
	    {
	    	showFileNoLoop(path);
	    }
	    catch(Exception e){}
		
		// Zadanie 4.
		
		System.out.println("\n\tZadanie 4");
		
		path = "../../napis.txt";
		
		showFile(path, 15464);
		
		// Zadanie 8.
		
		System.out.println("\n\tZadanie 8");
		
		BigInteger big = new BigInteger("-3000000000"); 
		BigInteger big1 = new BigInteger("3000000000"); 
		
		try 
		{ 
			int b1 = big.intValueExact(); 
		    int b2 = big1.intValueExact();
		} 
		catch (Exception e) { 
		    System.out.println("BigInteger exception: " + e); 
		} 
		
		// Zadanie 10.
		
		System.out.println("\n\tZadanie 10");
		
		GenericClass type = new GenericClass(App.class);
		
		showClassName(type);
    }
	
	static private void showFileNoLoop(String path) throws IOException
	{
		File file = new File(path);
		
		FileInputStream fis = new FileInputStream(file);
		
		byte[] data = new byte[(int) file.length()];
		
		fis.read(data);
		fis.close();

		String str = new String(data, "UTF-8");
		
		System.out.println("File '" + path + "' contentT '" + str + "'");
	}
	
	static private void showFile(String path)
	{
		BufferedReader reader;

        try
        {
            // Read line by line.

            reader = new BufferedReader(new FileReader(path));
            
            String input = "";

            String line = reader.readLine();
            
            input += line;

            while (line != null)
            {
                line = reader.readLine();
                
                if (line != null)
                {
                	input += "\n" + line;
                }
            }

            reader.close();
            
            System.out.println("File '" + path + "' content '" + input + "'");
        }
        catch (IOException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
	}
	
	static private void readFile(ArrayList<Integer> array, String path)
	{
		BufferedReader reader;

        try
        {
            // Read line by line.

            reader = new BufferedReader(new FileReader(path));

            String line = reader.readLine();

            while (line != null)
            {
                if (line != null)
                {
                    // Parse by space.

                    Scanner scanner = new Scanner(line);

                    while (scanner.hasNextInt())
                    {
                    	array.add(scanner.nextInt());
                    }
                }
                
                line = reader.readLine();
            }

            reader.close();
        }
        catch (IOException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
	
	static private void showFile(String path, int position)
	{
		BufferedReader reader;

        try
        {
            // Read line by line.

            reader = new BufferedReader(new FileReader(path));
            
            reader.skip(position);
            
            String input = "";

            String line = reader.readLine();
            
            input += line;

            while (line != null)
            {
                line = reader.readLine();
                
                if (line != null)
                {
                	input += "\n" + line;
                }
            }
            
            reader.close();
            
            System.out.println("File '" + path + "' content from "
            		+ Integer.toString(position) + " position '" + input + "'");
        }
        catch (IOException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
	}

	static private void showSum(ArrayList<Integer> array)
	{
		int sum = 0; 

	    for (int i : array)
	    {
	    	sum += i;
	    }

	    System.out.println("Array sum " + Integer.toString(sum));
	}
	
	static private void showClassName(GenericClass type)
	{
		String sortName= type.getType().getSimpleName();
		    
		System.out.println("Class name '" + sortName + "'");
	}
}
