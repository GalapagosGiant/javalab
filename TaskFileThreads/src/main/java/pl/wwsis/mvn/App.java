package pl.wwsis.mvn;

import java.util.*;
import java.time.*;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;


public class App 
{
	public static void main( String[] args )
    {
        // Create file.

        String path = "liczby.txt";

        createFile(path);

        // Fill file.

        fillFile(path);

        // Read file to array

        ArrayList<ArrayList<Integer>> array = new ArrayList<ArrayList<Integer>>();

        readFile(array, path);
        
        // TEST
        
        testNoThreads(array);
        
        testThreads(array, 1);
        testThreads(array, 2);
        testThreads(array, 4);
        testThreads(array, 8);
        testThreads(array, 16);
    }
	
	static private void testNoThreads(ArrayList<ArrayList<Integer>> array)
    {
		ArrayList<ArrayList<Integer>> copy = new ArrayList<ArrayList<Integer>>();;
		
		for( List<Integer> sublist : array) {
			copy.add(new ArrayList<Integer>(sublist));
		}
		
		Instant before = Instant.now();

        sortArray(copy);

        Instant after = Instant.now();

        long delta = Duration.between(before, after).toMillis();

        System.out.println("Sorting array time " + delta + " ms.");
    }
	
	static private void testThreads(ArrayList<ArrayList<Integer>> array, int number)
    {
		ArrayList<ArrayList<Integer>> copy = new ArrayList<ArrayList<Integer>>();;
		
		for( List<Integer> sublist : array) {
			copy.add(new ArrayList<Integer>(sublist));
		}
		
		ArrayList<SortThread> pool = new ArrayList<SortThread>();
		
		for (int i = 0; i < number; i++)
		{
			SortThread thr = new SortThread(copy);
			
			pool.add(thr);
		}
		
		Instant before = null, after = null;
		
		switch(number) 
		{
		  case 1:
			  pool.get(0).setRange(0, 15);
			  
			  break;
		  case 2:
			  pool.get(0).setRange(0, 7);
			  pool.get(0).setRange(8, 15);
			  
			  break;
		  case 4:

			  pool.get(0).setRange(0, 3);
			  pool.get(0).setRange(4, 7);
			  pool.get(0).setRange(8, 11);
			  pool.get(0).setRange(12, 15);
			  
			  break;
		  case 8:
			  
			  int j = 0;
			  
			  for (int i = 0; i < number; i++)
			  {
				  pool.get(i).setRange(j++, j++);
			  }
			  
			  break;
		  case 16:
			  
			  for (int i = 0; i < number; i++)
			  {
				  pool.get(i).setRange(i, i);
			  }
		    	  
			  break;
		  default:
			  return;
		}
		
		before = Instant.now();
        
		for (int i = 0; i < number; i++)
		{		    	  
			pool.get(i).start();
		}
		
		for (int i = 0; i < pool.size(); i++)
		{		    	  
			try {
				if (pool.get(i).isAlive())
				{
					pool.get(i).join();
				}
			} catch (Exception e) {
				
			}
		}
        
		after = Instant.now();
        
        long delta = Duration.between(before, after).toMillis();

        System.out.println("Sorting array " + number + " thread time " 
        				+ delta + " ms.");
    }
	 
    static private void createFile(String path)
    {
        try
        {
            File myObj = new File(path);

            if (myObj.createNewFile())
            {
                System.out.println("File '" + myObj.getName() + "' created.");
            }
            else
            {
                System.out.println("File '" + path + "' already exists.");
            }
        }
        catch (IOException e)
        {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
    }

    static private void fillFile(String path)
    {
        // Generate content.

        String content = "";

        for (int i = 0; i < 16; i++)
        {
            for (int j = 0; j < 1000; j++)
            {
                int num = ThreadLocalRandom.current().nextInt(0, 1000000);

                content += String.valueOf(num) + " ";
            }

            content += "\n";
        }

        // Write to file.

        try
        {
            FileWriter myWriter = new FileWriter(path);

            myWriter.write(content);
            myWriter.close();

            System.out.println("Successfully wrote content to the file.");
        }
        catch (IOException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    static private void readFile(ArrayList<ArrayList<Integer>> array, String path)
    {
        BufferedReader reader;

        try
        {
            // Read line by line.

            reader = new BufferedReader(new FileReader(path));

            String line = reader.readLine();

            while (line != null)
            {
                line = reader.readLine();

                if (line != null)
                {
                    // Parse by space.

                    ArrayList<Integer> elem = new ArrayList<Integer>();

                    Scanner scanner = new Scanner(line);

                    while (scanner.hasNextInt())
                    {
                        elem.add(scanner.nextInt());
                    }

                    array.add(elem);
                }
            }

            reader.close();
        }
        catch (IOException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    static private void sortArray(ArrayList<ArrayList<Integer>> array)
    {
        for (ArrayList<Integer> elem : array)
        {
            Collections.sort(elem);

//            for (int num : elem)
//            {
//                System.out.print(num + " ");
//            }
//            System.out.print("\n");
        }
    }
    
    static private void showArray(ArrayList<ArrayList<Integer>> array)
    {
    	for (ArrayList<Integer> elem : array)
        {
            for (int num : elem)
            {
                System.out.print(num + " ");
            }
            System.out.print("\n");
        }
    }
}
