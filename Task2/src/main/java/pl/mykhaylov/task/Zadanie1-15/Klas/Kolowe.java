
package pl.mykhaylov.task.Klas;

import pl.mykhaylov.task.Pojazd;

public class Kolowe extends Pojazd {

  String rozmiarOpony;

  public Kolowe()
  {
  }

  public Kolowe(String tireSize)
  {
    this.rozmiarOpony = tireSize;
  }

  public void uruchomSilnik(String type)
  {
    System.out.println("Kolowe: Uruchomiono silnik " + type + ".");
  }


  // Getters

  public String getTireSize()
  {
    return this.rozmiarOpony;
  }

  // Setter

  public void setTireSize(String size)
  {
    this.rozmiarOpony = size;
  }
}
