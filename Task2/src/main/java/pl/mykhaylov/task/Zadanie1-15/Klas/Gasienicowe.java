
package pl.mykhaylov.task.Klas;

import pl.mykhaylov.task.Pojazd;

public class Gasienicowe extends Pojazd {

  int moc;

  public Gasienicowe()
  {
  }

  public Gasienicowe(int strength)
  {
    this.moc = strength;
  }

  final public void uruchomSilnik()
  {
    super.uruchomSilnik();

    System.out.println("Gasienicowe: Jest to pojazd gasienicowy.");
  }

  // Getters

  public int getStrength()
  {
    return this.moc;
  }

  // Setter

  public void setStrength(int strength)
  {
    this.moc = strength;
  }
}
