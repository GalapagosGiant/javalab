
package pl.mykhaylov.task.Typ;

import pl.mykhaylov.task.Klas.Kolowe;

public class Osobowy extends Kolowe {

  int iloscOsob;

  public Osobowy()
  {
  }

  public Osobowy(int capacity)
  {
    this.iloscOsob = capacity;
  }

  // Getters

  public int getPeopleCapacity()
  {
    return this.iloscOsob;
  }

  // Setter

  public void setPeopleCapacity(int capacity)
  {
    this.iloscOsob = capacity;
  }
}
