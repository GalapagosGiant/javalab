
package pl.mykhaylov.task.Typ;

import pl.mykhaylov.task.Klas.Kolowe;

public class Ciezarowka extends Kolowe {

  int udzwig;

  public Ciezarowka()
  {
  }

  public Ciezarowka(int capacity)
  {
    this.udzwig = capacity;
  }

  public void podniesSkrzynie()
  {
  }

  // Getters

  public int getLiftingCapacity()
  {
    return this.udzwig;
  }

  // Setter

  public void setLiftingCapacity(int capacity)
  {
    this.udzwig = capacity;
  }
}
