
package pl.mykhaylov.task.Model;

import pl.mykhaylov.task.Marka.VW;

public class Golf extends VW {

  public Golf()
  {
  }

  public Golf(int year, int weight, String engineType, String color, String tireSize, int capacity)
  {
    setYear(year);
    setWeight(weight);
    setEngineType(engineType);
    setColor(color);
    setTireSize(tireSize);
    setPeopleCapacity(capacity);
  }

}
