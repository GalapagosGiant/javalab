
package pl.mykhaylov.task;

public class Pojazd {

  int rokProdukcji;
  int masa;

  String rodzajSilnika;
  String kolor;

  public Pojazd()
  {
  }

  public Pojazd(int year, int weight, String engineType, String color)
  {
    this.rokProdukcji = year;
    this.masa = weight;
    this.rodzajSilnika = engineType;
    this.kolor = color;
  }

  public void uruchomSilnik()
  {
    System.out.println("Pojazd: Silnik uruchomiono.");
  }

  // Getters

  public int getYear()
  {
    return this.rokProdukcji;
  }

  public int getWeight()
  {
    return this.masa;
  }

  public String getEngineType()
  {
    return this.rodzajSilnika;
  }

  public String getColor()
  {
    return this.kolor;
  }

  // Setter

  public void setYear(int year)
  {
    this.rokProdukcji = year;
  }

  public void setWeight(int weight)
  {
    this.masa = weight;
  }

  public void setEngineType(String type)
  {
    this.rodzajSilnika = type;
  }

  public void setColor(String color)
  {
    this.kolor = color;
  }

}
