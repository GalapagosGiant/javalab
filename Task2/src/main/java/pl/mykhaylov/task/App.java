
package pl.mykhaylov.task;

import java.util.Arrays;
import java.util.*;
import java.io.*;

import pl.mykhaylov.task.Model.Golf;
import pl.mykhaylov.task.Pojazd;
import pl.mykhaylov.task.Zadanie16.Prostokat;
import pl.mykhaylov.task.Zadanie17.Rownanie;
import pl.mykhaylov.task.Zadanie18.Tablica;
import pl.mykhaylov.task.Zadanie19.Sortowanie;

public class App 
{
  public static void main( String[] args )
  {
    // Zadanie 1 - 15.

    Pojazd golf1 = new Golf(2019, 1200, "elektryczny", "biały", "195/70 R15", 4);
    Pojazd golf2 = new Golf(2012, 1500, "benzynowy", "czerwony", "185/70 R16", 5);
    Pojazd golf3 = new Golf(2009, 1800, "diesel", "niebieski", "195/80 R17", 5);

    // Zadanie 16.

    Prostokat prostokat = new Prostokat();

    prostokat.czytajDane();
    prostokat.obliczPole();
    prostokat.pokazWynik();

    // Zadanie 17.

    Rownanie rownanie = new Rownanie();

    rownanie.czytajDane();
    rownanie.obliczPole();
    rownanie.pokazWynik();

    // Zadanie 18.

    double [][] macierz = new double[10][10];

    Tablica tablica = new Tablica();

    tablica.czytajDane(macierz, 10);
    tablica.obliczPole(macierz, 10);
    tablica.pokazWynik(macierz, 10);

    // Zadanie 19.

    int [] liczby = {4, 1, 2, 3};

    Sortowanie sortowanie = new Sortowanie();

    sortowanie.czytajDane(liczby, liczby.length);
    sortowanie.obliczPole(liczby, liczby.length);
    sortowanie.pokazWynik(liczby, liczby.length);

    // Zadanie NAPIS.txt.

    List<String> napisy = new ArrayList<String>();

    try
    {
      Scanner odczyt = new Scanner(new File("NAPIS.txt"));

      while (odczyt.hasNextLine())
      {
        String wiersz = odczyt.nextLine();

        napisy.add(wiersz);
      }
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }

    Collections.sort(napisy);

    String prev = napisy.get(0);;

    for (int i = 1; i < napisy.size(); i++)
    {
      String curr = napisy.get(i);

      if (curr.equals(prev))
      {
        System.out.print("NAPIS: " + curr + "\n");
      }
      else
      {
        prev = curr;
      }
    }
  }
}
