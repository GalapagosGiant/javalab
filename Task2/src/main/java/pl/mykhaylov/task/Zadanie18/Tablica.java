
package pl.mykhaylov.task.Zadanie18;

import java.util.Random;

public class Tablica {

  public Tablica()
  {
  }

  public void czytajDane(double [][] macierz, int rozmiar)
  {
    for (int i = 0; i < rozmiar; i++)
    {
      for (int j = 0; j < rozmiar; j++)
      {
        if (i == j)
        {
          Random rand = new Random();

          macierz[i][j] = rand.nextInt(10);
        }
        else
        {
          macierz[i][j] = 0;
        }
      }
    }
  }

  public void obliczPole(double [][] macierz, int rozmiar)
  {
    double suma = 0;

    for (int i = 0; i < rozmiar; i++)
    {
      suma += macierz[i][i];
    }

    System.out.print("Suma w tablice: " + suma + "\n");
  }

  public void pokazWynik(double [][] macierz, int rozmiar)
  {
    String wynik = "";

    for (int i = 0; i < rozmiar; i++)
    {
      for (int j = 0; j < rozmiar; j++)
      {
        wynik += macierz[i][j] + " ";
      }

      wynik += "\n";
    }

    System.out.print("Tablica: \n");
    System.out.print(wynik);
  }

}
