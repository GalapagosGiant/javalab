
package pl.mykhaylov.task.Zadanie17;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Rownanie {

  double pole;
  int a;
  int b;
  int c;

  double x1;
  double x2;
  int res = 0;

  public Rownanie()
  {
  }

  public void czytajDane()
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    System.out.print("Dane: Wprowadź 'a'.. ");

    try
    {
      this.a = Integer.parseInt(reader.readLine());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    System.out.print("Dane: Wprowadź 'b'.. ");

    try
    {
      this.b = Integer.parseInt(reader.readLine());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }

    System.out.print("Dane: Wprowadź 'c'.. ");

    try
    {
      this.c = Integer.parseInt(reader.readLine());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  public void obliczPole()
  {
    if (this.a == 0)
    {
      if (this.b == 0)
      {
        if (this.c == 0)
        {
          this.res = 0;
        }
        else
        {
          this.res = 0;
        }
      }
      else
      {
        this.x1 = -this.c / this.b;

        this.res = 1;
      }
    }
    else
    {
      double delta = this.b * this.b - 4 * this.a * this.c;

      if (delta < 0)
      {
        this.res = 0;
      }
      else
      {
        if (delta == 0)
        {
          this.x1 = -this.b / (2 * this.a);

          this.res = 1;
        }
        else
        {
          this.x1 = (-this.b + Math.sqrt(delta)) / (2 * this.a);
          this.x2 = (-this.b - Math.sqrt(delta)) / (2 * this.a);

          this.res = 2;
        }
      }
    }
  }

  public void pokazWynik()
  {
    if (this.res == 0)
    {
      System.out.println("Brak rozwiązań\n");
    }
    else if (this.res == 1)
    {
      System.out.print("Jedno podwójne rozwiązanie x = " + this.x1 + "\n");
    }
    else if (this.res == 2)
    {
      System.out.print("x1 = ");
      System.out.println(this.x1);
      System.out.print("x2 = ");
      System.out.println(this.x2);
    }
  }

}
