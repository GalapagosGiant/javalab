
package pl.mykhaylov.task.Zadanie19;

public class Sortowanie {

  public Sortowanie()
  {
  }

  public void czytajDane(int [] liczby, int n)
  {
    // ?
  }

  public void obliczPole(int [] liczby, int n)
  {
    int temp = 0;

    for (int i = 0; i < n; i++)
    {
      for (int j = 1; j < (n - i); j++)
      {
        if(liczby[j - 1] > liczby[j])
        {
          temp = liczby[j - 1];
          liczby[j - 1] = liczby[j];
          liczby[j] = temp;
        }
      }
    }
  }

  public void pokazWynik(int [] liczby, int n)
  {
    String wynik = "";

    for (int i = 0; i < n; i++)
    {
      wynik += String.valueOf(liczby[i]) + ", ";
    }

    System.out.print("Sortowanie: Wynik " + wynik + "\n");
  }

}
